import {  HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import countries from '../../_files/countries.json';

import { Ng2GoogleChartsModule, GoogleChartsSettings } from 'ng2-google-charts';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ValidateEqualModule } from 'ng-validate-equal';

import { SuiModule } from 'ng2-semantic-ui';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { RegisterationComponent } from './components/registeration/registeration.component';
import { UserSignupComponent } from './components/user-signup/user-signup.component';
import { UserSigninComponent } from './components/user-signin/user-signin.component';
import { HomeComponent } from './components/home/home.component';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';
import { FormsModule } from '@angular/forms';
import { AlertWarningComponent } from './components/alert-warning/alert-warning.component';
import { ContactComponent } from './components/contact/contact.component';
import { AboutComponent } from './components/about/about.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { PharmacyMainDashboardComponent } from './components/pharmacy-main-dashboard/pharmacy-main-dashboard.component';
import { AuthGuard } from './auth/auth.guard';
import { UserSigninServiceService } from './services/user-signin-service.service';
import { UserSignupServiceService } from './services/user-signup-service.service';
import { AuthInterceptor } from './auth/auth.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalComponent } from './components/modal/modal.component';

import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { RegisterFaceComponent } from './components/register-face/register-face.component';
import {WebcamModule} from 'ngx-webcam';
import { RecognizeFaceComponent } from './components/recognize-face/recognize-face.component';
import { CartComponent } from './components/cart/cart.component';
import { IntermediateformComponent } from './components/intermediateform/intermediateform.component';
import { DrugDetailsComponent } from './components/drug-details/drug-details.component';
import { DashboardDisplayComponent } from './components/dashboard-display/dashboard-display.component';
import { OrderMedicinesComponent } from './components/order-medicines/order-medicines.component';
import { OrderFitnessComponent } from './components/order-fitness/order-fitness.component';
import { OrderPersonalcareComponent } from './components/order-personalcare/order-personalcare.component';
import { OrderMomandbabyComponent } from './components/order-momandbaby/order-momandbaby.component';
import { OrderSexualwellnessComponent } from './components/order-sexualwellness/order-sexualwellness.component';
import { OrderDevicesComponent } from './components/order-devices/order-devices.component';
import { OrderCovidessentialsComponent } from './components/order-covidessentials/order-covidessentials.component';
import { MyactivityComponent } from './components/myactivity/myactivity.component';
import { AdminSigninComponent } from './components/admin-signin/admin-signin.component';
import { AdminSignupComponent } from './components/admin-signup/admin-signup.component';
import { AdminViewPageComponent } from './components/admin-view-page/admin-view-page.component';
import { AdminCompleteOrderStatusComponent } from './components/admin-complete-order-status/admin-complete-order-status.component';
import { AdminViewUserProfilesComponent } from './components/admin-view-user-profiles/admin-view-user-profiles.component';
import { CovidHomeComponent } from './components/covid-home/covid-home.component';
import { CovidCountrywiseComponent } from './components/covid-countrywise/covid-countrywise.component';
import { CovidDashboardComponent } from './components/covid-dashboard/covid-dashboard.component';
import { ChatComponent } from './components/chat/chat.component';
import { TawkService } from './services/tawk.service';






@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    RegisterationComponent,
    UserSignupComponent,
    UserSigninComponent,
    HomeComponent,
    PagenotfoundComponent,
    AlertWarningComponent,
    ContactComponent,
    AboutComponent,
    UserProfileComponent,
    PharmacyMainDashboardComponent,
    ModalComponent,
    RegisterFaceComponent,
    RecognizeFaceComponent,
    CartComponent,
    IntermediateformComponent,
    DrugDetailsComponent,
    DashboardDisplayComponent,
    OrderMedicinesComponent,
    OrderFitnessComponent,
    OrderPersonalcareComponent,
    OrderMomandbabyComponent,
    OrderSexualwellnessComponent,
    OrderDevicesComponent,
    OrderCovidessentialsComponent,
    MyactivityComponent,
    AdminSigninComponent,
    AdminSignupComponent,
    AdminViewPageComponent,
    AdminCompleteOrderStatusComponent,
    AdminViewUserProfilesComponent,
    CovidHomeComponent,
    CovidCountrywiseComponent,
    CovidDashboardComponent,
    ChatComponent
  ],
  imports: [
    BrowserModule,
    SuiModule,
    HttpClientModule,
    FormsModule,
    ValidateEqualModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatDialogModule,
    WebcamModule,
    Ng2GoogleChartsModule,

    MatButtonModule

  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  },
    AuthGuard,
    UserSigninServiceService,
    UserSignupServiceService,
    TawkService


  ],
  bootstrap: [AppComponent],

  entryComponents : [ModalComponent]

})
export class AppModule { }
