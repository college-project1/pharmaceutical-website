import { Router } from '@angular/router';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { UserSigninServiceService } from '../services/user-signin-service.service';
import { HandledEvent } from 'ng2-semantic-ui/dist';


@Injectable()

export class AuthInterceptor implements HttpInterceptor {

    constructor(private router : Router , private signin_service : UserSigninServiceService)
    {}

    intercept(req: HttpRequest<any>, next: HttpHandler){

        if(req.headers.get('NoAuth')){
            console.log('No aut req');
            // not receive any response as observable from nodejs as itis not an autheauthorised nticated req
            return next.handle(req.clone());
        }
        else{
            console.log(' aut req');


        const clonedreq = req.clone({
            headers : req.headers.set("Authorization" , "Bearer "+ this.signin_service.getToken() )
        });
        
        
        // req is sent  through next.handle() and from the backend it will receive a response as observable 
        return next.handle(clonedreq).pipe(

            tap(

                event =>{ console.log('token verification is successful') },
                err =>{

                            console.log('error caught in interceptor above token. the error is  :'+err.error['message'])

                    // if token verification fails (auth=False message =token not matched) OR (if error occurs) 
                    if(err.error.auth == false){
                        console.log('token verification not successful');
                        
                        this.router.navigateByUrl('/user_signin');
                    }

                    console.log('error caught in interceptor below token.the error is  : '+err)

                }
                // error close 
            )

        )



        }
        // else close 




    }

}