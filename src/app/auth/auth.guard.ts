import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserSigninServiceService } from '../services/user-signin-service.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

      constructor(private signin_service : UserSigninServiceService , private router : Router)
      {}



  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):boolean {
      // inside this we have to implement the logic to identify wheather the user is logged in or not
      // if logged in = return true   else return false 

      if(!this.signin_service.isLoggedIn()){
        this.router.navigateByUrl('/user_signin');
        this.signin_service.deleteToken();
        console.log('user is logged out');
        return false;
      }
      console.log('user is still logged in');
  
       return true;
  }
  
}
