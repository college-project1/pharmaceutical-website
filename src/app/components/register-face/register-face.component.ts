import { Component, OnDestroy, OnInit } from '@angular/core';
import { FaceserviceService } from '../../services/faceservice.service';
import { WebcamImage } from 'ngx-webcam';
import { Subject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { UserSigninServiceService } from '../../services/user-signin-service.service';



@Component({
  selector: 'app-register-face',
  templateUrl: './register-face.component.html',
  styleUrls: ['./register-face.component.css']
})
export class RegisterFaceComponent implements OnInit {

  face_detected : string;
  face_notdetected : string;
  loader : boolean;



  constructor(private face_service: FaceserviceService , private router : Router , private signin_service : UserSigninServiceService) { }


  ngOnInit(): void {

    console.log('in face ngonit component ')

    this.face_service.getFace_Authenticate().subscribe();

  }
  // ngonit() close 

  // latest snapshot
  public webcamImage: WebcamImage = null;

  // webcam snapshot trigge
  private trigger: Subject<void> = new Subject<void>();


  triggerSnapshot(): void {
    this.trigger.next();
  }

  handleImage(webcamImage: WebcamImage): void {
    console.info('received webcam image', webcamImage);
    this.webcamImage = webcamImage;

    // console.log(this.webcamImage.imageAsBase64);
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }


  onSubmit() {

    console.log('in sonsubmit()' + this.webcamImage);
    this.loader=true;

    // this.router.navigate(['/pharmacyMainDashboard']);      //pending statement here

    // get paylod id of user from local storage
    var get_payload = this.signin_service.getPayload();
    var user_id = get_payload._id;
    console.log(user_id);
    console.log(typeof(this.webcamImage.imageAsBase64 ))

    this.face_service.detectFace({'image' : this.webcamImage.imageAsBase64 , 'userid': user_id }).subscribe({

      next : (data)=>{
        
        this.loader=false;
        this.face_detected = data['message'];
        setTimeout(() => this.face_detected = '', 7000);

        console.log('face detected at last now inside submit func'+data['message'])
      },

      error : (err)=>{
        this.loader=false;

        this.face_notdetected = (err.error['message']);
        setTimeout(() => this.face_notdetected = '', 7000);
        
        console.log('error in detecting face '+ (err.error['message']));
      },

      complete : ()=>{
        this.loader=false;

          console.log('completed detecting face in face register component submit ()');
      }
      
      
      
    })
    // detectface 




  }















}
// export class close
