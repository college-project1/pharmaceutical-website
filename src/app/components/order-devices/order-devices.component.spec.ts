import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderDevicesComponent } from './order-devices.component';

describe('OrderDevicesComponent', () => {
  let component: OrderDevicesComponent;
  let fixture: ComponentFixture<OrderDevicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderDevicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
