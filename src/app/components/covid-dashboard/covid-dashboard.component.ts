import { Component, OnInit , Input } from '@angular/core';

@Component({
  selector: 'app-covid-dashboard',
  templateUrl: './covid-dashboard.component.html',
  styleUrls: ['./covid-dashboard.component.css']
})
export class CovidDashboardComponent implements OnInit {

  @Input('tot_confirmed') tot_confirmed;
  @Input('tot_recovered') tot_recovered;
  @Input('tot_deaths') tot_deaths;
  @Input('tot_active') tot_active;

  constructor() { }

  ngOnInit(): void {
  }

}
