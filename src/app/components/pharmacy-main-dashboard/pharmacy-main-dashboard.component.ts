import { Component, OnInit } from '@angular/core';
import { MaindashboardServiceService } from '../../services/maindashboard-service.service';

import { Router } from '@angular/router';
import { UserSigninServiceService } from '../../services/user-signin-service.service';
import { MedicineserviceService } from '../../services/medicineservice.service';

@Component({
  selector: 'app-pharmacy-main-dashboard',
  templateUrl: './pharmacy-main-dashboard.component.html',
  styleUrls: ['./pharmacy-main-dashboard.component.css']
})
export class PharmacyMainDashboardComponent implements OnInit {

  category_selected : string = 'displayadvertisement';
  prev_page_category : any;
  userName : string;

  constructor(private maindashboard_service : MaindashboardServiceService , public signin_service : UserSigninServiceService , private router : Router , private medicine_service : MedicineserviceService ) 
  { }

  ngOnInit(): void {

      // for authorization of maindashboard 
      console.log('in main dashbord ngonit component ')
      this.maindashboard_service.getPharmacyMainDashBoard_Authenticate().subscribe();


       
        this.prev_page_category = this.medicine_service.prev_page_category;
        console.log('prev page category : ',this.prev_page_category);

        this.category_selected = this.prev_page_category || 'displayadvertisement';

        if(this.medicine_service.is_home_btn_clicked){
        this.category_selected = 'displayadvertisement';
        this.medicine_service.is_home_btn_clicked = false;

        }


      console.log('prev page from service classssssssssss    :     ',this.medicine_service.prev_page_category);

      this.userDetails();




  
  }
  // ngonit close 

  Onselected_category(category : string){

    this.category_selected = category; 
    console.log('inside seecred category function ')
    console.log(this.category_selected)
    this.medicine_service.pageCategory(this.category_selected);
    console.log('returned back to on sekected category function component ')

 
  }

  logoutNow()
  {
    this.signin_service.deleteToken();
    this.medicine_service.deleteCartMedicines();

     
    this.prev_page_category = 'displayadvertisement';  
    console.log('prev page category now is :   ',this.prev_page_category)    //to display the advertisemnt page as the user logs in

    this.router.navigate(['/user_signin']);
  }

  userDetails(){

    var payload1 = this.signin_service.getPayload();
    console.log(payload1);
    this.userName = payload1['name'];

  }




}
// export class claose 
