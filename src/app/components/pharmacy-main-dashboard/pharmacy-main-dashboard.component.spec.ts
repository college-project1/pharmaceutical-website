import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PharmacyMainDashboardComponent } from './pharmacy-main-dashboard.component';

describe('PharmacyMainDashboardComponent', () => {
  let component: PharmacyMainDashboardComponent;
  let fixture: ComponentFixture<PharmacyMainDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PharmacyMainDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PharmacyMainDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
