import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderPersonalcareComponent } from './order-personalcare.component';

describe('OrderPersonalcareComponent', () => {
  let component: OrderPersonalcareComponent;
  let fixture: ComponentFixture<OrderPersonalcareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderPersonalcareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderPersonalcareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
