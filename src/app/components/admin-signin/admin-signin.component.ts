import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Admin_Signin_Class } from '../../models/admin_signin';
import { AdminserviceService } from '../../services/adminservice.service';

@Component({
  selector: 'app-admin-signin',
  templateUrl: './admin-signin.component.html',
  styleUrls: ['./admin-signin.component.css']
})
export class AdminSigninComponent implements OnInit {

  usermodel = new Admin_Signin_Class();
  serverErrorMessages : string;
  loader : any;

  constructor(private router : Router , private admin_service : AdminserviceService) { }

  ngOnInit(): void {
  }



  onLogin()
{
  this.loader = true;
  console.log(this.usermodel);
  // first authenticate 
  this.admin_service.adminAuthenticate(this.usermodel).subscribe({

    next : (result)=>{
      console.log(result);
      this.loader = false;
      

      // navigate to UserDash board page
      // this.router.navigateByUrl('/pharmacyMainDashboard');
      this.router.navigateByUrl('/adminViewPage');



    },

    error : (err) => {
      this.loader = false;
      this.serverErrorMessages = err.error.message;
      setTimeout(() => this.serverErrorMessages = '', 4000);

      console.log('unsuccessfull in fetching admin signin component   : '+ err.error.message)
    
    
    },
    
    complete: () => (console.log('successfully fetched details from backend in admin signin component'))




  })
    //  subscribe close 
}


}
