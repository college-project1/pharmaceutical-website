import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MedicineserviceService } from '../../services/medicineservice.service';

@Component({
  selector: 'app-drug-details',
  templateUrl: './drug-details.component.html',
  styleUrls: ['./drug-details.component.css']
})
export class DrugDetailsComponent implements OnInit {

  selected_MedDetail_obj : any;


  constructor(private medicine_service : MedicineserviceService , private route : Router) { }

  ngOnInit(): void {

    this.selected_MedDetail_obj = this.medicine_service.selected_medDetail_obj;
    console.log(this.selected_MedDetail_obj);

   



  }
// onit ends 

back(): void {
    this.route.navigateByUrl('/pharmacyMainDashboard');
}





}
// export class ends 
