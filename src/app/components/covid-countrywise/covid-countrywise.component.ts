import { Component, OnInit , ViewChild } from '@angular/core';
import countrywise_timeline_data from '../../_files/countrywise_timline_data.json';
import { GoogleChartInterface } from 'ng2-google-charts';
import countrywise_data from '../../_files/countrywise_covid_data.json';




@Component({
  selector: 'app-covid-countrywise',
  templateUrl: './covid-countrywise.component.html',
  styleUrls: ['./covid-countrywise.component.css']
})
export class CovidCountrywiseComponent implements OnInit {

  public countrywisetimeline_data : [] = countrywise_timeline_data.data;
  public countrywise_data : [] = countrywise_data.data;

  public country_array = [];
  selected_country : string;
  country_flags =[];
  selected_flag : any;

  tot_confirmed: number;
  tot_recovered: number;
  tot_deaths: number;
  tot_active : number = 0;

  confirmed_obj ;
  recovered_obj;
  deaths_obj;
  dates=[];
  no_of_updates : number;

  public lineChart: GoogleChartInterface = {
    chartType: 'LineChart'
  };

  public lineChart2: GoogleChartInterface = {
    chartType: 'LineChart'
  };

  public lineChart3: GoogleChartInterface = {
    chartType: 'LineChart'
  };



  constructor() { }

  @ViewChild('mychart ', {static: false}) mychart;
  @ViewChild('mychart2', {static: false}) mychart2;
  @ViewChild('mychart3', {static: false}) mychart3;



  ngOnInit(): void {

    console.log(this.countrywisetimeline_data);
    this.countrywisetimeline_data.forEach((ele)=>{
  
      this.country_array.push(ele['country'])

    });

    console.log(this.country_array);


    this.countrywise_data.forEach((ele)=>{

      this.country_flags.push(ele['countryInfo']['flag']);   

    })

    console.log('flags',this.country_flags);
    console.log(this.countrywise_data.length , this.country_flags.length)







    this.update_country('Afghanistan');
    this.no_of_updates = 0;


  }
  // ngonit 

  update_country(country){

    console.log(country);
    this.selected_country = country;
    this.no_of_updates = this.no_of_updates + 1;
    
    var index = this.country_array.indexOf(country);
    console.log(index);
    this.selected_flag = this.country_flags[index];
    console.log(this.selected_flag);

    var country_info_obj = this.countrywisetimeline_data[index]['timeline'];
    console.log(country_info_obj);

     this.confirmed_obj = country_info_obj['cases'];
     this.recovered_obj = country_info_obj['recovered'];
     this.deaths_obj = country_info_obj['deaths'];

    var keys = Object.keys(this.confirmed_obj);
    this.dates = keys;
    var last = keys[keys.length-1];

    console.log(last);

    this.tot_confirmed = this.confirmed_obj[last];
    this.tot_recovered = this.recovered_obj[last];
    this.tot_deaths = this.deaths_obj[last];

    console.log(this.tot_confirmed,this.tot_recovered,this.tot_deaths);



    

    if(this.no_of_updates >1){
      this.updatechart();
    }
    else{
      this.init_updatechart();
    }

  // flag 

  







  }
  // county 


  updatechart(){

    var datatable1 = [];
    var datatable2 = [];
    var datatable3 = [];

    datatable1.push(['dates', 'confirmed' ]);
    datatable2.push(['dates', 'recovered' ]);
    datatable3.push(['dates', 'deaths' ]);


    this.dates.forEach((date)=>{

      datatable1.push([date , this.confirmed_obj[date]]);
      datatable2.push([date , this.recovered_obj[date]]);
      datatable3.push([date , this.deaths_obj[date]]);
      

    })
    // for loop 

    this.lineChart = {
      chartType: 'LineChart',
      dataTable: datatable1,

      options: {
        height: 500,
        width : 500,
        lineWidth: 3,
        colors : ['midnightblue'],
        


        is3D: true,
              animation: {
        duration: 1000,
        easing: 'out',
        startup: true
      }
      },
    };

    this.lineChart2 = {
      chartType: 'LineChart',
      dataTable: datatable2,

      options: {
        height: 500,
        width : 500,
        lineWidth: 3,
        colors : ['midnightblue'],
       

        is3D: true,
              animation: {
        duration: 1000,
        easing: 'out',
        startup: true
      }
      },
    };

    this.lineChart3 = {
      chartType: 'LineChart',
      dataTable: datatable3,


      options: {
        height: 500,
        width : 500,
      
        lineWidth: 3,
        colors : ['red'],

        is3D: true,
              animation: {
        duration: 1000,
        easing: 'out',
        startup: true
      }
      },
    };
  
    this.mychart.draw();
    this.mychart2.draw();
    this.mychart3.draw();





  }
  // updatechart 


  init_updatechart(){

    var datatable1 = [];
    var datatable2 = [];
    var datatable3 = [];

    datatable1.push(['dates', 'confirmed' ]);
    datatable2.push(['dates', 'recovered' ]);
    datatable3.push(['dates', 'deaths' ]);



    this.dates.forEach((date)=>{

      datatable1.push([date , this.confirmed_obj[date]]);
      datatable2.push([date , this.recovered_obj[date]]);
      datatable3.push([date , this.deaths_obj[date]]);
      

    })
    // for loop 

    this.lineChart = {
      chartType: 'LineChart',
      dataTable: datatable1,

      options: {
        height: 500,
        width : 500,
        lineWidth: 3,
        colors : ['black'],
        

        is3D: true,
              animation: {
        duration: 1000,
        easing: 'out',
        startup: true
      }
      },
    };

    this.lineChart2 = {
      chartType: 'LineChart',
      dataTable: datatable2,

      options: {
        height: 500,
        width : 500,
        lineWidth: 3,
        colors : ['midnightblue'],
        is3D: true,
              animation: {
        duration: 1000,
        easing: 'out',
        startup: true
      }
      },
    };

    this.lineChart3 = {
      chartType: 'LineChart',
      dataTable: datatable3,

      options: {
        height: 500,
        width : 500,
        // legend: 'none',
        lineWidth: 3,
        colors : ['red'],

        is3D: true,
              animation: {
        duration: 1000,
        easing: 'out',
        startup: true
      }
      },
    };
  



  }
  // updatechart 


}
