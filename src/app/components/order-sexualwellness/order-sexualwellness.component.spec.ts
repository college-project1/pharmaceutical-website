import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderSexualwellnessComponent } from './order-sexualwellness.component';

describe('OrderSexualwellnessComponent', () => {
  let component: OrderSexualwellnessComponent;
  let fixture: ComponentFixture<OrderSexualwellnessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderSexualwellnessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderSexualwellnessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
