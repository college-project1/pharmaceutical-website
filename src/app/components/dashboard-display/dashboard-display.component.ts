import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthenticateprivateresourcesService } from '../../services/authenticateprivateresources.service';
import { TawkService } from 'src/app/services/tawk.service';

@Component({
  selector: 'app-dashboard-display',
  templateUrl: './dashboard-display.component.html',
  styleUrls: ['./dashboard-display.component.css']
})
export class DashboardDisplayComponent implements OnInit,OnDestroy {

  constructor(private authenticateservice : AuthenticateprivateresourcesService,private TawkService:TawkService) { }

  ngOnInit(): void {
    this.TawkService.SetChatVisibility(true);
    
    this.authenticateservice.Authenticate().subscribe();

  }
  ngOnDestroy(){
    this.TawkService.SetChatVisibility(false);
  }

}
