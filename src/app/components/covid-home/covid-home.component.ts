import { Component, OnInit, ViewChild } from '@angular/core';
import total_cases from '../../_files/total_covid_cases.json';
import countrywise_data from '../../_files/countrywise_covid_data.json';
// import { GoogleChartInterface } from 'ng2-google-charts';

import {
  ChartReadyEvent,
  ChartErrorEvent,
  ChartSelectEvent,
  ChartMouseOverEvent,
  ChartMouseOutEvent,
  RegionClickEvent,
  GoogleChartInterface,
  GoogleChartComponent,
  GoogleChartsControlInterface,
  GoogleChartsDashboardInterface,
  GoogleChartEditor,
  GoogleChartWrapper,
} from 'ng2-google-charts';



@Component({
  selector: 'app-covid-home',
  templateUrl: './covid-home.component.html',
  styleUrls: ['./covid-home.component.css']
})
export class CovidHomeComponent implements OnInit {

  public total_cases : any = total_cases;
  public countrywise_data : [] = countrywise_data.data;

  tot_confirmed : number = 0;
  tot_active : number = 0;
  tot_recovered : number = 0;
  tot_deaths : number = 0;

  datatable = [];
  

  public pieChart: GoogleChartInterface = {
    chartType: 'PieChart'
  };
  
  public lineChart: GoogleChartInterface = {
    chartType: 'LineChart'
  };

  constructor() { }

  @ViewChild('mychart ', {static: false}) mychart;
  @ViewChild('mychart2 ', {static: false}) mychart2;


  ngOnInit(): void {

    console.log(this.total_cases);
    console.log(this.countrywise_data);

    this.tot_active = this.total_cases.active;
    this.tot_confirmed = this.total_cases.cases;
    this.tot_deaths = this.total_cases.deaths;
    this.tot_recovered = this.total_cases.recovered;


    this.updatechartinit('a')


  }
  // ngonit

  public updatechart(case_type : String ){
    console.log(case_type);


    var datatable2 = [];
    datatable2.push(['country', case_type ]);

    this.countrywise_data.forEach((ele) => {

      let value: Number = 0;

      if (case_type === 'c') {

        if (ele['cases'] > 500000) {
          value = ele['cases'];
        }

      }

      else if (case_type === 'r') {

        if (ele['recovered'] > 500000) {
          value = ele['recovered'];
        }

      }

      else if (case_type === 'd') {

        if (ele['deaths'] > 25000) {
          value = ele['deaths'];
        }

      }

      else if (case_type === 'a') {

        if (ele['active'] > 100000) {
          value = ele['active'];
        }

      }

      if(value > 0){

        datatable2.push([ele['country'], value]);
      }



    });
    // for loop
  this.datatable = datatable2;

  // console.log('data table2 is    :  ',   this.datatable);


    this.pieChart = {
      chartType: 'PieChart',
      dataTable: this.datatable,

      options: {
        height: 500,
        width : 750,
       
        is3D: true,
              animation: {
        duration: 1000,
        easing: 'out',
        startup: true
      }
      },
    };

    this.lineChart = {
      chartType: 'LineChart',
      dataTable: this.datatable,

      options: {
        height: 500,
        width : 850,
        lineWidth: 2.5,
        // colors : ['midnightblue'],
        is3D: true,
              animation: {
        duration: 1000,
        easing: 'out',
        startup: true
      }
      },
    };

    if(case_type === 'a'){
      this.lineChart.options.colors = ['green'];
    }
    if(case_type === 'd'){
      this.lineChart.options.colors = ['red'];
    }
    if(case_type === 'r'){
      this.lineChart.options.colors = ['midnightblue'];
    }
    if(case_type === 'c'){
      this.lineChart.options.colors = ['black'];
    }

    console.log('piechart data table   : ');
    console.log(this.pieChart.dataTable.length);
    console.log('this.mychart      :' , this.mychart)
    this.mychart.draw();
    this.mychart2.draw();

    // this.updatechartinit(case_type);
    


  }
// update chart

public updatechartinit(case_type : String ){
  console.log(case_type);


  var datatable2 = [];
  datatable2.push(['country', case_type ]);

  this.countrywise_data.forEach((ele) => {

    let value: Number = 0;

    if (case_type === 'c') {

      if (ele['cases'] > 500000) {
        value = ele['cases'];
      }

    }

    else if (case_type === 'r') {

      if (ele['recovered'] > 500000) {
        value = ele['recovered'];
      }

    }

    else if (case_type === 'd') {

      if (ele['deaths'] > 25000) {
        value = ele['deaths'];
      }

    }

    else if (case_type === 'a') {

      if (ele['active'] > 100000) {
        value = ele['active'];
      }

    }

    if(value > 0){

      datatable2.push([ele['country'], value]);
    }



  });
  // for loop
this.datatable = datatable2;

// console.log('data table2 is    :  ',   this.datatable);


  this.pieChart = {
    chartType: 'PieChart',
    dataTable: this.datatable,

    options: {
      height: 500,
      width : 750,
      
      is3D: true,
      animation: {
        duration: 1000,
        easing: 'out',
        startup: true
      }
    },
  };

  this.lineChart = {
    chartType: 'LineChart',
    dataTable: this.datatable,
    

    options: {
      height: 500,
      width : 950,
      lineWidth: 2.5,
      colors : ['midnightblue'],

      is3D: true,
      animation: {
        duration: 1000,
        easing: 'out',
        startup: true
      }
    },
  };

  if(case_type === 'a'){
    this.lineChart.options.colors = ['green'];
  }
  if(case_type === 'd'){
    this.lineChart.options.colors = ['red'];
  }
  if(case_type === 'r'){
    this.lineChart.options.colors = ['midnightblue'];
  }
  if(case_type === 'c'){
    this.lineChart.options.colors = ['black'];
  }

  console.log('piechart data table   : ');
  console.log(this.pieChart.dataTable.length);
  console.log('this.mychart      :' , this.mychart);

  // this.updatechart(case_type);

}
// update chart

onchange(k){
  console.log('change');
}






}
