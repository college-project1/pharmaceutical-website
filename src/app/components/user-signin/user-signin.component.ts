import { Component, OnDestroy, OnInit } from '@angular/core';
import { User_Signin } from '../../models/user_signin';
import { UserSigninServiceService } from '../../services/user-signin-service.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-user-signin',
  templateUrl: './user-signin.component.html',
  styleUrls: ['./user-signin.component.css']
})
export class UserSigninComponent implements OnInit{

  usermodel = new User_Signin();
  serverErrorMessages : string;
  loader : boolean;

  constructor(private signin_service : UserSigninServiceService , private router : Router ) { }

  ngOnInit(): void {
  
    const still_logged_in = this.signin_service.isLoggedIn()
    if(still_logged_in){
      this.router.navigateByUrl('/pharmacyMainDashboard');
    }

  }

onLogin()
{
  this.loader = true;
  console.log(this.usermodel);
  // first authenticate 
  this.signin_service.userAuthenticate(this.usermodel).subscribe({

    next : (result)=>{
  this.loader = false;

      console.log(result);
      this.signin_service.storeToken(result['token']);

      // navigate to UserDash board page
      // this.router.navigateByUrl('/pharmacyMainDashboard');
      this.router.navigateByUrl('/pharmacyMainDashboard');



    },

    error : (err) => {
  this.loader = false;

      this.serverErrorMessages = err.error.message;
      setTimeout(() => this.serverErrorMessages = '', 4000);

      console.log('unsuccessfull in fetching token from backend in user signin component   : '+ err.error.message)
    
    
    },
    
    complete: () => (console.log('successfully fetched thetoken from backend in user signin component'))




  })
    //  subscribe close 
}





}
// class close 
