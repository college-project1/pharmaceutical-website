import { Component, OnInit } from '@angular/core';
import { AdminserviceService } from '../../services/adminservice.service';
import { UserSigninServiceService } from '../../services/user-signin-service.service';

@Component({
  selector: 'app-admin-view-page',
  templateUrl: './admin-view-page.component.html',
  styleUrls: ['./admin-view-page.component.css']
})
export class AdminViewPageComponent implements OnInit {

  pendingOrderDetails_arrayOfObjects: any = [];
  remarks : string;
  is_order_cancelled : boolean;
  warning : boolean;
  order_selected : any;
  loader : boolean;
  validity_days : number;
  orderid_selected_validity : any;
  validity_selected : boolean;

  constructor(private admin_service : AdminserviceService ) { }

  ngOnInit(): void {



    this.displayPendingOrderDetails()

  }
  // ngonit 

validityFun(outer_ele : any){
  console.log(outer_ele._id)
  this.validity_days = undefined;

  this.validity_selected = true;

  this.orderid_selected_validity = outer_ele._id;
}

  displayPendingOrderDetails()
{

  this.loader=true;

  this.admin_service.getPendingMedicineDetails().subscribe({

    next : (result) =>{
        this.loader=false;


        console.log('successfullu fetched the pending details');
        console.log(result);
        this.pendingOrderDetails_arrayOfObjects = result['message'];

        this.pendingOrderDetails_arrayOfObjects.forEach(ele => {
          ele.prev_order_details[0].order_date = new Date( ele.prev_order_details[0].order_date).toDateString();
          
        });

    },

    error : (err) =>{

        this.loader=false;

      console.log('failed to fetch the pending details');
      console.log(err.error['message']);

       
    },
    complete : ()=>{
      console.log('completed  the pending details part');


    }


  })


}
// display pending order details 


  formVisiblity(ele : any){

    this.is_order_cancelled = true;
    this.order_selected = ele;


  }

  approveOrder(order_obj: any){
    this.loader = true;

    console.log('approved order  is ',order_obj);

    // update the properties 

    this.is_order_cancelled = false;
    order_obj.status = 'approved';
    order_obj.prev_order_details[0].validity_in_days = this.validity_days;

    console.log('approved order details ',order_obj)

    this.admin_service.approveOrder(order_obj).subscribe({




      next : (result) =>{
    this.loader = false;


        console.log('successfullu aprroved the order of the pending details');
        console.log(result);
        this.displayPendingOrderDetails();
      
    },

    error : (err) =>{

    this.loader = false;

      console.log('failed to approve the pending details');
      console.log(err.error['message']);
      this.displayPendingOrderDetails();


       
    },
    complete : ()=>{
      console.log('completed  the approved details part');


    }



    })



  }
  // approve order 

  cancelOrder(order_obj: any){

    this.loader = true;

    console.log('cancelled order  is ',order_obj);
    order_obj['remarks'] = this.remarks;
    // this.is_order_cancelled = true;
    console.log('remarks : ',this.remarks);

    if(this.remarks){
      
      order_obj.status = 'cancelled';

      this.admin_service.cancelOrder(order_obj).subscribe({




        next : (result) =>{

    this.loader = false;

  
          console.log('successfull cancelled the order of the pending details');
          console.log(result);
        this.displayPendingOrderDetails();

          
        
      },
  
      error : (err) =>{

    this.loader = false;
        
        console.log('failed to cancel the pending details');
        console.log(err.error['message']);
        this.displayPendingOrderDetails();

  
         
      },
      complete : ()=>{
        console.log('completed  the cancelled details part');
  
  
      }
  
  
  
      })
  
  


    }
    // if ends 

    else{

        this.warning=true;
        setTimeout(()=>{
          this.warning =false;
        },3000)
    }

   

  }
// cancel order 


addQuantity(outer_ele_id : any ,ele_id:any){

console.log(outer_ele_id);

console.log(ele_id);
this.pendingOrderDetails_arrayOfObjects.forEach(ext_ele => {

  if(ext_ele._id === outer_ele_id){

    (ext_ele.prev_order_details[0].total_medicine_details).forEach(ele =>{
  
      if(ele._id === ele_id){
        ele.quantity_added = ele.quantity_added + 1;
        ele.total_amount =  ele.medicine_details.amount * ele.quantity_added  ;
        ele.total_amount = (ele.total_amount);

        // recalculate the total amount of the cart 
       
        ext_ele.total_cartamount = Number(ext_ele.total_cartamount )+ Number(ele.medicine_details.amount) ;
        ext_ele.total_cartamount = Number(ext_ele.total_cartamount);

        console.log('updated' ,ext_ele)



      }
    })


  }
  
});


}
// addQuantity 



subQuantity(outer_ele_id : any ,ele_id:any ,present_quantity : number){

console.log(outer_ele_id);

console.log(ele_id)
console.log(present_quantity);

if(present_quantity > 1){

      
  this.pendingOrderDetails_arrayOfObjects.forEach(ext_ele => {

    if(ext_ele._id === outer_ele_id){
  
      (ext_ele.prev_order_details[0].total_medicine_details).forEach(ele =>{
    
        if(ele._id === ele_id){
          ele.quantity_added = ele.quantity_added - 1;
          ele.total_amount =  ele.medicine_details.amount * ele.quantity_added  ;
          ele.total_amount = (ele.total_amount);
  
          // recalculate the total amount of the cart 
         
          ext_ele.total_cartamount = Number(ext_ele.total_cartamount ) - Number(ele.medicine_details.amount) ;
          ext_ele.total_cartamount = Number(ext_ele.total_cartamount);
  
          console.log('updated' ,ext_ele)
  
  
  
        }
      })
  
  
    }
    
  });








}
// present quantity if 

  


}
// subQuantity 


}
// export class 