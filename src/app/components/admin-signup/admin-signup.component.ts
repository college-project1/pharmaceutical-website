import { Router } from '@angular/router';
import { Admin_Signup_Class } from './../../models/admin_signup';
import { Component, OnInit } from '@angular/core';
import { AdminserviceService } from '../../services/adminservice.service';

@Component({
  selector: 'app-admin-signup',
  templateUrl: './admin-signup.component.html',
  styleUrls: ['./admin-signup.component.css']
})
export class AdminSignupComponent implements OnInit {

  usermodel = new Admin_Signup_Class();
  serverErrorMessages : string;
  invalidity: boolean;
  validity: boolean;
  loader: boolean;
  showSucessMessage: boolean;
  

  constructor(private router : Router , private admin_service : AdminserviceService) { }

  ngOnInit(): void {
  }


  credentials_validation(): boolean {


    if (this.usermodel.full_name == "" || this.usermodel.full_name == undefined) {

      this.invalidity = true;
      return this.invalidity;
    }
    if ((/\d/).test(this.usermodel.full_name)) {
      if (this.usermodel.full_name != '') {

        this.invalidity = true;
        console.log('name contains digit')
        return this.invalidity;

      }
    }

    if (!(/^[A-Za-z]+/).test(this.usermodel.full_name)) {
      this.invalidity = true;
      console.log('starting of name in invalid')
      return this.invalidity;

    }


    if (this.usermodel.email == '' || this.usermodel.email == undefined) {
      this.invalidity = true;
      return this.invalidity;
    }
    if (!(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/).test(this.usermodel.email)) {
      this.invalidity = true;
      return this.invalidity;

    }


    if (this.usermodel.password == '' || this.usermodel.password == undefined || this.usermodel.password.length < 8) {
      console.log("empty", this.usermodel.password)
      this.invalidity = true;
      return this.invalidity;
    }



    if (!(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/).test(this.usermodel.password)) {
      console.log("email pattern invalid")

      this.invalidity = true;
      return this.invalidity;

    }

    if (this.usermodel.password !== this.usermodel.confirmPassword) {
      console.log("confirm password is invalid")
      this.invalidity = true;
      return this.invalidity;
    }

    console.log('outside of credential validation1')
    // this.validity = true;
    return false;


  }





form_validation() {
  console.log(this.usermodel);

  console.log('entered formvalidation fun')

 

  if (!this.credentials_validation()) {
    console.log('valid app')
    
    console.log('entering backend validation fun')
    this.loader = true;

    this.admin_service.postUser(this.usermodel).subscribe({

      next: (result) => {

        this.loader = false;
        console.log('successfull backend call')
        this.validity = true;
        this.invalidity = false;
        console.log('invalidity is', this.invalidity);
        console.log('validity is', this.validity);
  
        this.showSucessMessage = true;
        setTimeout(() => this.showSucessMessage = false, 3000);
        // setTimeout(() => this.validity = false, 5000);
        setTimeout(() => {

          this.validity = false;
          this.router.navigateByUrl('/adminSignin');
          

          
        }, 5000);
       
      },

      error: (err) => {

        this.loader = false;

        console.log('unsuccessful backend call')
        this.invalidity = true;
        setTimeout(() => this.invalidity = false, 5000);

        if (err.status === 422) {
          this.serverErrorMessages = err.error.join('<br/>');
          setTimeout(() => this.serverErrorMessages = '', 4000);
        }
        else {
          this.serverErrorMessages = 'Something went wrong.Please contact admin.';
          setTimeout(() => this.serverErrorMessages = '', 4000);
          
        }
      },

      complete: () => (console.log('successfully fetched the details for register from backend in user signup component'))

    })


  }

  else {
    console.log('invalid app ')
    this.invalidity = true;
    this.validity = false;
    console.log('invalidity is', this.invalidity);
    console.log('validity is', this.validity);
    setTimeout(() => this.invalidity = false, 5000);


  }





}
// formValidation close 








}
// export class