import { Component, OnInit } from '@angular/core';
import { Cart } from '../../models/cart';
import { CartserviceService } from '../../services/cartservice.service';
import { Router } from '@angular/router';
import { async } from '@angular/core/testing';
import { AuthenticateprivateresourcesService } from '../../services/authenticateprivateresources.service';
import { MedicineserviceService } from '../../services/medicineservice.service';
import { UserSigninServiceService } from '../../services/user-signin-service.service';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {


  usermodel = new Cart();
  hiddeninput: any;
  controlled_med_array: any = [];
  uncontrolled_med_array: any = [];
  total_med_id_array: any[];
  message: string;
  medicine_selected: any;
  total_amount_cart: number =0;
  imageSrc : String ='';
  messagePrescription : String = '';


  constructor(private cartservice: CartserviceService, private router: Router, private authenticateservice: AuthenticateprivateresourcesService, private medicine_service: MedicineserviceService , private signin_service : UserSigninServiceService) { }

  ngOnInit(): void {

    console.log('in  cart  ngonit component ')

    console.log('above call component ')

    // this.authenticateservice.Authenticate().subscribe();

    console.log('below call component ')


    this.controlled_med_array = this.medicine_service.cartMedicines_controlled_array;
    this.uncontrolled_med_array = this.medicine_service.cartMedicines_uncontrolled_array;
    this.total_med_id_array = this.medicine_service.cartMedicines_id_array;

    console.log(this.medicine_service.cartMedicines_controlled_array)

    console.log(this.controlled_med_array, this.uncontrolled_med_array);

    this.totalAmount_Cart();




  }
  // onit close


  addQuantity(ele: any) {

    this.medicine_selected = ele;
    console.log('in add medivibe func')

    if (this.uncontrolled_med_array.includes(ele)) {

      console.log('adding the quntity of uncontyrooled med ')

      var indx = this.uncontrolled_med_array.indexOf(ele);
      this.uncontrolled_med_array[indx].quantity_added = this.uncontrolled_med_array[indx].quantity_added + 1;
      this.uncontrolled_med_array[indx].total_amount = this.uncontrolled_med_array[indx].medicine_details.amount * this.uncontrolled_med_array[indx].quantity_added;
      this.uncontrolled_med_array[indx].total_amount = (this.uncontrolled_med_array[indx].total_amount).toFixed(2);
      this.totalAmount_Cart();
    }

    else {
      console.log('not adding the quntity of uncontyrooled med ')

      this.message = 'Quantity is restricted to 1 only !';
      setTimeout(() => {
        this.message = '';
      }, 3000);

    }

  }
  // add quantiyy ends 

  subQuantity(ele: any) {

    this.medicine_selected = ele;
    console.log('in sub medivibe func')

    if (this.uncontrolled_med_array.includes(ele)) {

      console.log('subs the quntity of uncontrolled med ')

      if (ele.quantity_added > 1) {

        console.log('subs the quntity of uncontrolled med whose quntity >1 ')

        var indx = this.uncontrolled_med_array.indexOf(ele);
        this.uncontrolled_med_array[indx].quantity_added = this.uncontrolled_med_array[indx].quantity_added - 1;
        this.uncontrolled_med_array[indx].total_amount = this.uncontrolled_med_array[indx].medicine_details.amount * this.uncontrolled_med_array[indx].quantity_added;
        this.uncontrolled_med_array[indx].total_amount = (this.uncontrolled_med_array[indx].total_amount).toFixed(2);
        this.totalAmount_Cart();





      }

      else {

        console.log('removing ofuncontrolled med whose quntity = 1 ')


        var indx = this.uncontrolled_med_array.indexOf(ele);
        this.uncontrolled_med_array.splice(indx, 1);

        var indx2 = this.total_med_id_array.indexOf(ele.medicine_details._id);
        this.total_med_id_array.splice(indx2,1);

        this.totalAmount_Cart();



      }


    }

    else {

      console.log('removing of controlled med whose quntity = 1 ')


      var indx = this.controlled_med_array.indexOf(ele);
      this.controlled_med_array.splice(indx, 1);

      var indx2 = this.total_med_id_array.indexOf(ele.medicine_details._id);
        this.total_med_id_array.splice(indx2,1);

      this.totalAmount_Cart();

    }


  }
  // subquantity ends


removeMedicine(ele){

    console.log('in remove medicine');

    if(this.uncontrolled_med_array.includes(ele)){

        var indx = this.uncontrolled_med_array.indexOf(ele);
        this.uncontrolled_med_array.splice(indx,1);

        var indx2 = this.total_med_id_array.indexOf(ele.medicine_details._id);
        this.total_med_id_array.splice(indx2,1);

        this.totalAmount_Cart();

    }
    else{


      var indx = this.controlled_med_array.indexOf(ele);
      this.controlled_med_array.splice(indx, 1);

      var indx2 = this.total_med_id_array.indexOf(ele.medicine_details._id);
      this.total_med_id_array.splice(indx2,1);


      this.totalAmount_Cart();

    }

    console.log('med id array',this.total_med_id_array);


}



  totalAmount_Cart() {

    console.log('inside total amount cart')
    var tot_amt: number = 0;

    this.uncontrolled_med_array.forEach(ele => {

      tot_amt = tot_amt + parseInt(ele.total_amount);


    });

    this.controlled_med_array.forEach(ele => {

      tot_amt = tot_amt + parseInt(ele.total_amount);

    });

    this.total_amount_cart = (tot_amt);

    console.log("total amount is   :  ", this.total_amount_cart)


  }
  // total amount 

  inputPrescription(event : any){

    var k = event.target.files[0];
      // console.log(event.target.files[0]);
      const reader = new FileReader();
      reader.onload = (e: any) => {
     

         this.imageSrc = e.target.result;
     
        console.log('image is          :   ',  typeof(this.imageSrc));
      };

     
      reader.readAsDataURL(event.target.files[0]);
      
     

     
    

  }
  // pres ends 



  Paynow() {


    //check if prescription is needed or not if yes then accept

    if(this.controlled_med_array.length >0 ){
      if(this.imageSrc !== ''){
    
      this.createControlledMedicineDeatils();
    this.router.navigateByUrl('/recognizeFace');



      }

      else{
        this.messagePrescription = 'Please upload  Prescription first !';
        setTimeout(() => {
          this.messagePrescription = '';
        }, 3000);
      }
    }
    else{

      // unconrolled medicines 
      this.createMedicineDeatils();

      console.log('r going into  payment of uncontolled med details')
      

      this.medicine_service.paymentFor_UncontrolledMedicine().subscribe({

          next : (result)=>{
            console.log('in next()');
            console.log(result['message'])
            console.log('retured back fron payment of uncontolled med details');
            this.PaymentGateway();

          },

          error : (err)=>{
            console.log('in error()');
            console.log('in error()',err.error);


          },

          complete : ()=>{
            console.log('in complete().... after storing prev order in');

          }
      })
      
// this.router.navigateByUrl('/recognizeFace');

    }
  }
  // paynow 


  // creating medicine details for uncontrolled drugs 
  createMedicineDeatils(){

      console.log("in create medicine detils")

    var email_id = this.signin_service.getPayload()['email'];
    var name = this.signin_service.getPayload()['name'];
    var controlled_medicine_id_array : any =[];
    var uncontrolled_medicine_id_array : any =[];
    var totalMedicinesDetails_arrayOfObjects : any = [];

    this.controlled_med_array.forEach(ele => {

      controlled_medicine_id_array.push(ele.medicine_details._id);
      totalMedicinesDetails_arrayOfObjects.push(ele);
      
    });

    this.uncontrolled_med_array.forEach(ele => {

      uncontrolled_medicine_id_array.push(ele.medicine_details._id);
      totalMedicinesDetails_arrayOfObjects.push(ele);

      
    });

   var temp_obj ={
        
              email : email_id,
              prev_order_details :[
                {
                  order_date : new Date(),
                  status : 1,
                  controlled_med_id : controlled_medicine_id_array,
                  uncontrolled_med_id : uncontrolled_medicine_id_array,
                  total_medicine_details : totalMedicinesDetails_arrayOfObjects
                }
              ]


    }
    // temp_obj 
    console.log(temp_obj);
    console.log(name);
    console.log(this.total_amount_cart);
    console.log('type of total amt', typeof(this.total_amount_cart));

    
    this.medicine_service.finalCartMedicineDetails(name,this.total_amount_cart,temp_obj);

    console.log('returning to paynow funtion');



    
    //no need for face vrification
    // 1) store data in Dbse and proceed for payment


  }
  // create details 



   // creating medicine details for uncontrolled drugs 
   createControlledMedicineDeatils(){

    console.log("in create controlled medicine detils")

  var email_id = this.signin_service.getPayload()['email'];
  var name_id = this.signin_service.getPayload()['name'];
  var controlled_medicine_id_array : any =[];
  var uncontrolled_medicine_id_array : any =[];
  var totalMedicinesDetails_arrayOfObjects : any = [];

  this.controlled_med_array.forEach(ele => {

    controlled_medicine_id_array.push(ele.medicine_details._id);
    totalMedicinesDetails_arrayOfObjects.push(ele);
    
  });

  this.uncontrolled_med_array.forEach(ele => {

    uncontrolled_medicine_id_array.push(ele.medicine_details._id);
    totalMedicinesDetails_arrayOfObjects.push(ele);

    
  });

 var temp_obj ={
      
            email : email_id,
            name : name_id,
            status : 'pending',
            prescriptionUrl : this.imageSrc,
            total_cartamount : this.total_amount_cart,
            prev_order_details :[
              {
                order_date : new Date(),
                controlled_med_id : controlled_medicine_id_array,
                uncontrolled_med_id : uncontrolled_medicine_id_array,
                total_medicine_details : totalMedicinesDetails_arrayOfObjects
              }
            ]


  }
  // temp_obj 
  console.log(temp_obj);
  console.log(name_id);
  console.log(this.total_amount_cart);
  console.log('type of total amt', typeof(this.total_amount_cart));

  
  this.medicine_service.finalCartMedicineDetails(name_id,this.total_amount_cart,temp_obj);

  console.log('returning to create controlled medicine  funtion');



  
  //no need for face vrification
  // 1) store data in Dbse and proceed for payment


}
// create controlled medicine details  details 




  PaymentGateway() {
    console.log('inside paymentgateway ()');
    var payment_details_obj = {

      email : this.signin_service.getPayload()['email'],
      name : this.signin_service.getPayload()['name'],
      amount : this.total_amount_cart
    }

    console.log('payment details obj',payment_details_obj);
    this.cartservice.CartDetails(payment_details_obj).subscribe({

      next: (result) => {
        console.log('inside next()of paynow and storing valing in hidden input making visi on ')
        console.log(result)

        this.hiddeninput = result['hiddeninput'];

        console.log(result['hiddeninput'])

        console.log('going inside hidden input')
        this.cartservice.HiddenInputs(this.hiddeninput);
        console.log('out of hidden input service calss .going to route next component')


        this.router.navigateByUrl('/intermediateForm');

      },
      error: (err) => {
        console.log(err)
      },
      complete: () => {
        console.log('successfully fetched details in paynow()')
      }


    })


  }
  // paynow 

  showMedicineDetails(medicineDetails: any) {

    console.log('showdetails', medicineDetails);
    this.medicine_service.showMedicineDetails(medicineDetails);
    this.router.navigateByUrl('/medicineDetails');



  }




}
// export class close