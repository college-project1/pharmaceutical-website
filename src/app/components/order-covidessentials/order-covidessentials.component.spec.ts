import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderCovidessentialsComponent } from './order-covidessentials.component';

describe('OrderCovidessentialsComponent', () => {
  let component: OrderCovidessentialsComponent;
  let fixture: ComponentFixture<OrderCovidessentialsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderCovidessentialsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderCovidessentialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
