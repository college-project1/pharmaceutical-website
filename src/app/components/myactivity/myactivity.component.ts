import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { UserSigninServiceService } from 'src/app/services/user-signin-service.service';
import { MyactivityserviceService } from '../../services/myactivityservice.service';
import { MedicineserviceService } from '../../services/medicineservice.service';
import { CartserviceService } from '../../services/cartservice.service';
import { GoogleChartInterface } from 'ng2-google-charts';


@Component({
  selector: 'app-myactivity',
  templateUrl: './myactivity.component.html',
  styleUrls: ['./myactivity.component.css']
})
export class MyactivityComponent implements OnInit {

  category_selected : string ;
  prevOrderDetails_arrayOfObjects : any = [] ;
  pendingOrderDetails_arrayOfObjects : any = [];
  approvedOrderDetails_arrayOfObjects : any = [];
  cancelledOrderDetails_arrayOfObjects : any = [];
  hiddeninput : any;
  datatable1 = [];
  loader : boolean;

  public lineChart: GoogleChartInterface = {
    chartType: 'ColumnChart'
};

public lineChart2: GoogleChartInterface = {
  chartType: 'LineChart'
};

  constructor(private myactivity_service : MyactivityserviceService ,  private signin_service : UserSigninServiceService ,private router : Router , private medicine_service : MedicineserviceService , private cartservice: CartserviceService ) { }

  ngOnInit(): void {

  // pending status from admin
  // complete status from prev order

  this.category_selected = 'completed';
  this.completed();


  }
  // onit 


displayChart(){
  console.log('inside display chrt')

  var datatable =[];
  datatable.push(['Date','Amount']);

  this.prevOrderDetails_arrayOfObjects.forEach((ele)=>{


    var date = new Date(ele['order_date']);
    var dateString = date.toISOString().slice(0, 10);

    var total_amount: number = 0;

    ele['total_medicine_details'].forEach((med)=>{
      total_amount = total_amount + med['total_amount'];
    })

  datatable.push([dateString , total_amount]);
  this.datatable1 = datatable;

  })
  // for loop   
 console.log('datatable    : ', datatable);

this.lineChart ={
  chartType: 'ColumnChart',

  dataTable : this.datatable1,
    options: {

      legend: 'none',
          colors: ['#156650'], 
      height: 600,
      width : 900,

      is3D: true,
            animation: {
      duration: 1000,
      easing: 'out',
      startup: true
    }

  }
};

this.lineChart2 ={
  chartType: 'LineChart',

  dataTable : this.datatable1,
    options: {
      height: 600,
      width : 600,

      is3D: true,
            animation: {
      duration: 1000,
      easing: 'out',
      startup: true
    }

  }
};





}
// display chart 






  showMedicineDetails(medicineDetails: any) {

    console.log('showdetails', medicineDetails);
    this.medicine_service.showMedicineDetails(medicineDetails);
    this.router.navigateByUrl('/medicineDetails');

  }

  Category(cat : string){

  this.category_selected = cat;

  if(this.category_selected === 'completed')
  {
    console.log('category selected   :', this.category_selected);
    this.completed();
  }

  else if(this.category_selected === 'pending-approval'){
    console.log('category selected   :', this.category_selected);

    this.pendingApproved();


  }

  else if(this.category_selected === 'approved'){
    console.log('category selected   :', this.category_selected);

    this.approved();
    
  }

  else if(this.category_selected === 'cancelled'){
    console.log('category selected   :', this.category_selected);

    this.cancelled();
    
  }



  }

// category ends 

completed(){

  this.loader = true;

  var email_id = this.signin_service.getPayload()['email'];

    this.myactivity_service.getPrevOrderDetails(email_id).subscribe({


      next : (result)=>{
        console.log('in next()');
        this.loader = false;

        this.prevOrderDetails_arrayOfObjects = result['message'];
        this.prevOrderDetails_arrayOfObjects.forEach(ele => {
          ele.order_date = new Date(ele.order_date).toDateString();
          
        });

        this.prevOrderDetails_arrayOfObjects.reverse();

        console.log('prevOrderDetails_arrayOfObjects',this.prevOrderDetails_arrayOfObjects);


        this.displayChart();

      

      },

      error : (err)=>{
        this.loader = false;

        console.log('in error()');
        console.log('in error()',err.error);


      },

      complete : ()=>{
        console.log('in complete().... after fetching prev order details ');

      }
    })
}
// completed 

// ---------------------------------------------------------------------------------------------------------------------
pendingApproved(){

  this.loader = true;


  var email_id = this.signin_service.getPayload()['email'];

  this.myactivity_service.getPendingMedicines({email : email_id}).subscribe({



    next : (result)=>{

      this.loader = false;

      console.log('in next()');
      console.log(result);


      console.log('successfullu fetched the user pending details')
      console.log(result);
      this.pendingOrderDetails_arrayOfObjects = result['message'];

      this.pendingOrderDetails_arrayOfObjects.forEach(ele => {
        ele.prev_order_details[0].order_date = new Date( ele.prev_order_details[0].order_date).toDateString();
        
      });

      this.pendingOrderDetails_arrayOfObjects.reverse();
    

    },

    error : (err)=>{
      this.loader = false;

      console.log('in error()');
      console.log('in error()',err.error);


    },

    complete : ()=>{
      console.log('in complete().... after fetching user prev order details ');
    }

  })




}
// pendingApproved 

// -------------------------------------------------------------------------------------------------------------------------


approved(){

  
  this.loader = true;


  var email_id = this.signin_service.getPayload()['email'];

  this.myactivity_service.getApprovedMedicines({email : email_id}).subscribe({



    next : (result)=>{

      this.loader = false;

      console.log('in next()');
      console.log(result);


      console.log('successfullu fetched the user approved details')
      console.log(result);
      this.approvedOrderDetails_arrayOfObjects = result['message'];

      this.approvedOrderDetails_arrayOfObjects.forEach(ele => {
        ele.prev_order_details[0].order_date = new Date( ele.prev_order_details[0].order_date).toDateString();
        
      });
    
      this.approvedOrderDetails_arrayOfObjects.reverse();

    },

    error : (err)=>{
      this.loader = false;

      console.log('in error() in approved orers');
      console.log('in error()',err.error);


    },

    complete : ()=>{
      console.log('in complete().... after fetching user approved  order details ');
    }

  })

}
// approved 
cancelled(){

  
  this.loader = true;


  var email_id = this.signin_service.getPayload()['email'];

  this.myactivity_service.getCancelledMedicines({email : email_id}).subscribe({



    next : (result)=>{

      this.loader = false;

      console.log('in next()');
      console.log(result);


      console.log('successfullu fetched the user cancelled details')
      console.log(result);
      this.cancelledOrderDetails_arrayOfObjects = result['message'];

      this.cancelledOrderDetails_arrayOfObjects.forEach(ele => {
        ele.prev_order_details[0].order_date = new Date( ele.prev_order_details[0].order_date).toDateString();
        
      });

      this.cancelledOrderDetails_arrayOfObjects.reverse();
    

    },

    error : (err)=>{
      this.loader = false;

      console.log('in error() in cancelled order');
      console.log('in error()',err.error);


    },

    complete : ()=>{
      console.log('in complete().... after fetching user cancelled order details ');
    }

  })
}
// cancelled 



Paynow(outer_ele : any){
  this.loader = true;

console.log(outer_ele);
outer_ele.prev_order_details[0]['status'] = 1;
let temp_obj ={
        
  email : outer_ele.email,
  prev_order_details :[
    outer_ele.prev_order_details[0]
  ]
  
}
console.log('temp obj :  ',temp_obj);
console.log('orderid :  ', outer_ele._id)

this.myactivity_service.convertAndStoreApprovedDetailsToCompleteDetails(temp_obj , outer_ele._id).subscribe({


  next : (result)=>{

    // this.loader = false;
    

    console.log('in next()');
    console.log('successfullu updated approved->cpmplete t details')
    console.log(result);
    this.PaymentGateway(outer_ele);

    

  },

  error : (err)=>{
    this.loader = false;

    console.log('in error() in approve->complete order');
    console.log('in error()',err.error);


  },

  complete : ()=>{
    console.log('in complete().... after converting approved->complete order details ');
  }



})
// subscribe 



}
// paynow ends 


PaymentGateway(outer_ele : any) {
  console.log('inside paymentgateway ()');
  var payment_details_obj = {

    email : this.signin_service.getPayload()['email'],
    name : this.signin_service.getPayload()['name'],
    amount : outer_ele.total_cartamount
  }

  console.log('payment details obj',payment_details_obj);
  this.cartservice.CartDetails(payment_details_obj).subscribe({

    next: (result) => {
      console.log('inside next()of paynow and storing valing in hidden input making visi on ')
      console.log(result)

      this.hiddeninput = result['hiddeninput'];

      console.log(result['hiddeninput'])

      console.log('going inside hidden input')
      this.cartservice.HiddenInputs(this.hiddeninput);
      console.log('out of hidden input service calss .going to route next component')


      this.router.navigateByUrl('/intermediateForm');

    },
    error: (err) => {
      console.log(err)
    },
    complete: () => {
      console.log('successfully fetched details in paynow()')
    }


  })


}
// paynow 


}
// class
