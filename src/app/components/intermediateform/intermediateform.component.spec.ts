import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntermediateformComponent } from './intermediateform.component';

describe('IntermediateformComponent', () => {
  let component: IntermediateformComponent;
  let fixture: ComponentFixture<IntermediateformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntermediateformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntermediateformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
