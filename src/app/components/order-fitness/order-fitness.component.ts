import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MedicineserviceService } from '../../services/medicineservice.service';

@Component({
  selector: 'app-order-fitness',
  templateUrl: './order-fitness.component.html',
  styleUrls: ['./order-fitness.component.css']
})
export class OrderFitnessComponent implements OnInit {

  medicine_array : any;
  cartMedicines_id_array : any =[];
  message : string;
  status : boolean;
  med_selected_inview_id : any;
  // disable_AddtoCart_button :  boolean ;
  // medicine_selected : any;
  // medicine_selected_array : any = [];

  constructor(private medicine_service : MedicineserviceService , private router: Router ) { }

  ngOnInit(): void {
  
    console.log('fitness component')

  this.medicine_service.getMedicines({'category' : 'fitness'}).subscribe({

  next : (result)=>{
    console.log('succefully fetched data in next()');
    console.log(result);
    
   
    console.log(result['message']);
    
    this.medicine_array = result['message'];
    this.cartMedicines_id_array = this.medicine_service.cartMedicines_id_array;

    
  },

  error : (err)=>{
    console.log('errror in add to cart ');
    console.log(err.error)
  },

  complete : ()=>{
    console.log('in complete in addto cart ')
  }


  })
// subscribe ends 

  }
  // ngonit ends 


  addToCart(medicine_added_obj: any){
  
    console.log('in add to cart function in component : ',medicine_added_obj);
    console.log('going inside add to cart in service');
    // this.medicine_service.addMedicinesToCart(medicine_added_obj['_id'] , medicine_added_obj);

    this.medicine_service.addMedicinesToCart(medicine_added_obj['_id'] , medicine_added_obj).subscribe({

      next : (result)=>{
        console.log('succefully fetched data in next() ',  );
        console.log(result);

       this.message = result['message'];
       this.status =result['status'];
       this.med_selected_inview_id = medicine_added_obj['_id'];

       setTimeout(() => {

        this.message = '';
         
       }, 5000);



         console.log('message is :   ',this.message);
        
    
        this.cartMedicines_id_array = this.medicine_service.cartMedicines_id_array;
        
        
      },
    
      error : (err)=>{
        console.log('errror in fetching category details ');
        console.log(err.error)
      },
    
      complete : ()=>{
        console.log('in complete in category medi')
      }




    })
    // subscribe close 


    // console.log('returning back from add to cart from service');
    
    // this.cartMedicines_id_array = this.medicine_service.cartMedicines_id_array;
    
  }
  // addtocart 


  showMedicineDetails(medicineDetails:any){

    console.log('showdetails',medicineDetails);
    this.medicine_service.showMedicineDetails(medicineDetails);
    this.router.navigateByUrl('/medicineDetails');



  }

}
