import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderFitnessComponent } from './order-fitness.component';

describe('OrderFitnessComponent', () => {
  let component: OrderFitnessComponent;
  let fixture: ComponentFixture<OrderFitnessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderFitnessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderFitnessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
