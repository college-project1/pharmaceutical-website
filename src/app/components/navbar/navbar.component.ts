import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { UserSigninServiceService } from '../../services/user-signin-service.service';
import { MedicineserviceService } from '../../services/medicineservice.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  is_cart_empty : any;
  constructor(public signin_service : UserSigninServiceService , private router : Router , public medicine_service : MedicineserviceService) { }

  ngOnInit(): void {

    
  }

  logoutNow()
  {
    this.signin_service.deleteToken();
    this.router.navigate(['/user_signin']);
  }

  showadvertisementpage(val : boolean)
  {
    this.medicine_service.showHomepage(val);
  }

}
