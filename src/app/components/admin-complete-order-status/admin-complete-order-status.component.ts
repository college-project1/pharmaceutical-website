import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminserviceService } from '../../services/adminservice.service';

@Component({
  selector: 'app-admin-complete-order-status',
  templateUrl: './admin-complete-order-status.component.html',
  styleUrls: ['./admin-complete-order-status.component.css']
})
export class AdminCompleteOrderStatusComponent implements OnInit {

  loader: boolean;
  loader2 : boolean;
  prevOrderDetails_arrayOfObjects: any = [];
  status_selected : number;
  selected_outer_ele_id : any;
  email_selected : string;

  constructor(private route: ActivatedRoute, private admin_service: AdminserviceService , private router : Router   ) { }

  ngOnInit(): void {

    console.log('in init()')
    this.getUserPreviousOrderHistoryDetails();

  }
  // onit 

  getUserPreviousOrderHistoryDetails() {

    this.route.paramMap.subscribe((params) => {

      let email = params.get('email');
      this.email_selected =email;
      console.log('email of user for history details :', email);
      this.getUserCompletedHistoryDetails(email);

    })


  }
  // getUsersPreviousOrderHistoryDeatils 

  getUserCompletedHistoryDetails(email_id: string) {

    this.loader = true;


    this.admin_service.getUserCompletedStatusDetails(email_id).subscribe({


      next: (result) => {
        console.log('in next()');
        this.loader = false;

        this.prevOrderDetails_arrayOfObjects = result['message'];
        this.prevOrderDetails_arrayOfObjects.forEach(ele => {
          ele.order_date = new Date(ele.order_date).toDateString();

        });

        this.prevOrderDetails_arrayOfObjects.reverse();

        console.log('prevOrderDetails_arrayOfObjects', this.prevOrderDetails_arrayOfObjects);

        



      },

      error: (err) => {
        this.loader = false;

        console.log('in error()');
        console.log('in error()', err.error);


      },

      complete: () => {
        console.log('in complete().... after fetching prev order details ');

      }
    })
  }
  // completed 

  statusSelected(status : number , outer_ele : any){
    console.log(status , outer_ele);
    this.status_selected = status;
    this.selected_outer_ele_id = outer_ele._id;


  }
  // statusSelected 

  updateStatus(){

    this.loader2 = true;

  let temp_obj={
    email: this.email_selected,
    _id : this.selected_outer_ele_id,
    status : Number(this.status_selected)

  }
  console.log('status sent is   : ',temp_obj);

  this.admin_service.updateStatusAndDate(temp_obj).subscribe({


    next: (result) => {
      this.loader2 = false;
      console.log('in next() :  ',result);
      //reload the same component 


    
    },

    error: (err) => {
      this.loader2 = false;

      console.log('in error()');
      console.log('in error()', err.error);
      //reload the same component 


    },

    complete: () => {
      console.log('in complete().... after updateing status and date ');
      //reload the same component 
      let currentUrl = this.router.url;
      console.log({currentUrl})
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate([currentUrl]);

    }

  })
// subscribe 
  }

}
// export