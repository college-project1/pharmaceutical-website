import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MatDialog , MatDialogConfig } from '@angular/material/dialog';
import { User_Signup_Class } from '../../models/user_signup';
import { UserSignupServiceService } from '../../services/user-signup-service.service';
import countries from '../../_files/countries.json';
import { ModalComponent } from '../modal/modal.component';



@Component({
  selector: 'app-user-signup',
  templateUrl: './user-signup.component.html',
  styleUrls: ['./user-signup.component.css']
})
export class UserSignupComponent implements OnInit {

  // countrylist = countries ;
  public country_state : any =countries;
  usermodel = new User_Signup_Class();
  is_password_same: boolean = true;
  invalidity: boolean;
  validity: boolean;
  state_array: string[];
  city_array: string[];
  state_city_object: any;
  is_state_selected_error: boolean = false;
  is_city_selected_error: boolean = false;
  serverErrorMessages: string;
  showSucessMessage: boolean;
  loader : boolean;


  constructor(private signup_service: UserSignupServiceService , public matDialog : MatDialog , private router : Router   ) { }

  ngOnInit(): void {

      //setting state city from static file
        this.state_city_object = this.country_state;
        this.state_array = Object.keys(this.country_state);



  }
  // ngonit() close 


  
  openModal() {
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = false;
    dialogConfig.id = "modal-component";
    dialogConfig.height = "250px";
    dialogConfig.width = "600px";
    // https://material.angular.io/components/dialog/overview
    const modalDialog = this.matDialog.open(ModalComponent, dialogConfig);
  }






  // ----------------------------------------------------------


  selectedState(state: string) {
    console.log(state)
    if (state === 'none') {
      this.is_state_selected_error = true;
    }
    else {
      this.is_state_selected_error = false;
      this.usermodel.city = 'none';
      this.city_array = this.state_city_object[state]
      console.log(this.city_array)
    }


  }
  // selectedstate() close -------------------------------------here we had selected STATE and the corresponding CITIES

  selectedState_onblur(state_value: string) {
    if (state_value === 'none') {
      this.is_state_selected_error = true;
    }
    else {
      this.is_state_selected_error = false;
    }


  }
  // -----------------------------selected state onblur()-----------------------------


  selectedCity(city_value: string) {
    if (city_value === 'none') {
      this.is_city_selected_error = true;
    }
    else {
      this.is_city_selected_error = false;
    }

  }
  // selected city() close -----------------------------------------------------------------

  credentials_validation(): boolean {


    if (this.usermodel.full_name == "" || this.usermodel.full_name == undefined) {

      this.invalidity = true;
      return this.invalidity;
    }
    if ((/\d/).test(this.usermodel.full_name)) {
      if (this.usermodel.full_name != '') {

        this.invalidity = true;
        console.log('name contains digit')
        return this.invalidity;

      }
    }

    if (!(/^[A-Za-z]+/).test(this.usermodel.full_name)) {
      this.invalidity = true;
      console.log('starting of name in invalid')
      return this.invalidity;

    }


    if (this.usermodel.email == '' || this.usermodel.email == undefined) {
      this.invalidity = true;
      return this.invalidity;
    }
    if (!(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/).test(this.usermodel.email)) {
      this.invalidity = true;
      return this.invalidity;

    }


    if (this.usermodel.password == '' || this.usermodel.password == undefined || this.usermodel.password.length < 8) {
      console.log("empty", this.usermodel.password)
      this.invalidity = true;
      return this.invalidity;
    }



    if (!(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/).test(this.usermodel.password)) {
      console.log("email pattern invalid")

      this.invalidity = true;
      return this.invalidity;

    }

    if (this.usermodel.password !== this.usermodel.confirmPassword) {
      console.log("confirm password is invalid")
      this.is_password_same = false;
      this.invalidity = true;
      return this.invalidity;
    }

    if (!(/^\d{10}$/).test((this.usermodel.phone).toString())) {
      console.log("phone pattern invalid")

      this.invalidity = true;
      return this.invalidity;

    }

    if (this.usermodel.address == '' || this.usermodel.address == undefined) {
      console.log("addeess is empty")

      this.invalidity = true;
      return this.invalidity;
    }

    if ((/^[\s]+/).test(this.usermodel.address)) {
      console.log('address contains space');
      this.invalidity = true;
      return this.invalidity;
    }

    if (this.usermodel.state === 'none' || this.usermodel.city === 'none') {
      console.log('either state or city not selected');
      this.invalidity = true;
      return this.invalidity;

    }
    if (!(/^\d{6}$/).test((this.usermodel.pin).toString())) {
      console.log('pin invalid');
      this.invalidity = true;
      return this.invalidity;
    }

    console.log('outside of credential validation1')
    // this.validity = true;
    return false;

  }
  // credentialvalidation() close 
  // ------------------------------------------------------------------------------------------------------------------------------



  form_validation() {
    console.log(this.usermodel);

    console.log('entered formvalidation fun')

   

    if (!this.credentials_validation()) {
      console.log('valid app')
      
      console.log('entering backend validation fun')
      this.loader = true;

      this.signup_service.postUser(this.usermodel).subscribe({

        next: (result) => {

          this.loader = false;
          console.log('successfull backend call')
          this.validity = true;
          this.invalidity = false;
          console.log('invalidity is', this.invalidity);
          console.log('validity is', this.validity);
    
          this.showSucessMessage = true;
          setTimeout(() => this.showSucessMessage = false, 3000);
          // setTimeout(() => this.validity = false, 5000);
          setTimeout(() => {

            this.validity = false;
            this.router.navigateByUrl('/user_signin');
            

            
          }, 5000);
         
        },

        error: (err) => {

          this.loader = false;

          console.log('unsuccessful backend call')
          this.invalidity = true;
          setTimeout(() => this.invalidity = false, 5000);

          if (err.status === 422) {
            this.serverErrorMessages = err.error.join('<br/>');
            setTimeout(() => this.serverErrorMessages = '', 4000);
          }
          else {
            this.serverErrorMessages = 'Something went wrong.Please contact admin.';
            setTimeout(() => this.serverErrorMessages = '', 4000);
            
          }
        },

        complete: () => (console.log('successfully fetched the details for register from backend in user signup component'))

      })


    }
    else {
      console.log('invalid app ')
      this.invalidity = true;
      this.validity = false;
      console.log('invalidity is', this.invalidity);
      console.log('validity is', this.validity);
      setTimeout(() => this.invalidity = false, 5000);


    }





  }
  // formValidation close 




}
