import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderMomandbabyComponent } from './order-momandbaby.component';

describe('OrderMomandbabyComponent', () => {
  let component: OrderMomandbabyComponent;
  let fixture: ComponentFixture<OrderMomandbabyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderMomandbabyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderMomandbabyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
