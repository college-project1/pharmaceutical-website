import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MedicineserviceService } from '../../services/medicineservice.service';


@Component({
  selector: 'app-order-momandbaby',
  templateUrl: './order-momandbaby.component.html',
  styleUrls: ['./order-momandbaby.component.css']
})
export class OrderMomandbabyComponent implements OnInit {

  medicine_array : any;


  constructor(private medicine_service : MedicineserviceService , private router: Router ) { }

  ngOnInit(): void {
  
  

  this.medicine_service.getMedicines({'category' : 'mom and baby'}).subscribe({

  next : (result)=>{
    console.log('succefully fetched data in next() ')
    console.log(result);
   
    console.log(result['message']);
    
    this.medicine_array = result['message'];
    
  },

  error : (err)=>{
    console.log('errror in fetching category details ');
    console.log(err.error)
  },

  complete : ()=>{
    console.log('in complete in category medi')
  }


  })
// subscribe ends 

  }
  // ngonit ends 


  addToCart(medicine_added_array: any){
  
    console.log('add to cart function');
    console.log(medicine_added_array);
  }

  showMedicineDetails(medicineDetails:any){

    console.log('showdetails',medicineDetails);
    this.medicine_service.showMedicineDetails(medicineDetails);
    this.router.navigateByUrl('/medicineDetails');



  }



}
// export 
