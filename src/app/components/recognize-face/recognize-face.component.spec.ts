import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecognizeFaceComponent } from './recognize-face.component';

describe('RecognizeFaceComponent', () => {
  let component: RecognizeFaceComponent;
  let fixture: ComponentFixture<RecognizeFaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecognizeFaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecognizeFaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
