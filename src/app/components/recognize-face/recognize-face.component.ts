import { Component, OnInit } from '@angular/core';
import { FaceserviceService } from '../../services/faceservice.service';
import { WebcamImage } from 'ngx-webcam';
import { Subject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { UserSigninServiceService } from '../../services/user-signin-service.service';
import { MedicineserviceService } from '../../services/medicineservice.service';




@Component({
  selector: 'app-recognize-face',
  templateUrl: './recognize-face.component.html',
  styleUrls: ['./recognize-face.component.css']
})
export class RecognizeFaceComponent implements OnInit {

  face_detected : string;
  face_notdetected : string;
  errorMessage_whileStoringPendingDetailsInDb : string;
  loader : boolean;


  constructor(  private face_service: FaceserviceService , private router : Router ,private signin_service : UserSigninServiceService , private med_service : MedicineserviceService    ) { }

  ngOnInit(): void {

    console.log('in Recognize face ngonit component ')

    this.face_service.getFace_Authenticate().subscribe();

  }
  // ngonit close 

// latest snapshot
public webcamImage: WebcamImage = null;

// webcam snapshot trigge
private trigger: Subject<void> = new Subject<void>();


triggerSnapshot(): void {
  this.trigger.next();
}

handleImage(webcamImage: WebcamImage): void {
  console.info('received webcam image', webcamImage);
  this.webcamImage = webcamImage;

  // console.log(this.webcamImage.imageAsBase64);
}

public get triggerObservable(): Observable<void> {
  return this.trigger.asObservable();
}


onSubmit(){
  console.log('inside submit of recg face')


  this.loader=true;
  var get_payload = this.signin_service.getPayload();
  var user_id = get_payload._id;
  console.log(user_id);
  console.log(typeof(this.webcamImage.imageAsBase64 ))

  this.face_service.recognizeFace(  {'image' : this.webcamImage.imageAsBase64 , 'userid': user_id } ).subscribe({

    next : (data)=>{
      
      this.loader=false;
      this.face_detected = `${data['message']} with ${data['data_details'].confidence}% accuracy.`;
      setTimeout(() => 
      {
        this.face_detected = '';
    
    }, 3000);


      console.log('successfully recognized face inside submit');
      console.log(data);
      console.log(this.med_service.cartMedicines_id_array);
      console.log('final medicibe object for controlled drug',this.med_service.medicine_obj);
      
      // save the pending status data in data base 
      
      this.med_service.storePendingStatusDetails_forAdminVerification(this.med_service.medicine_obj).subscribe({

        next : (data)=>{

          console.log(data);
          console.log('successfully stored the pending details in the data base');

          // navigating to my activity for the current status 
          setTimeout(() => {
          this.router.navigateByUrl('/myActivity');
            
          }, 2000);
        },


        error : (err)=>{

          console.log('failed to store the pending details in the data base');
          this.errorMessage_whileStoringPendingDetailsInDb = 'Something went wrong ! Please try after sometime.';
         

          // again do the facial recognization part so goto cart page
          setTimeout(() => {

              this.face_detected = '';
              setTimeout(() => {

                this.errorMessage_whileStoringPendingDetailsInDb ='';
                this.router.navigateByUrl('/cart');
                
              }, 3000);
              
              

            
          }, 2000);
    
        }, 

        complete : () => {
          console.log('completed the pending ststud part ');
        }

      })
      // subscribe inner ends 


    },

    error : (err)=>{

      this.loader=false;
      this.face_notdetected = err.error['message'];
      setTimeout(() => {
       this.face_notdetected = '';
      this.router.navigateByUrl('/cart');

      }, 3000
       );
      console.log('unsuccessfully recognized face in onsubmit');
      console.log(err.error['message'])
      console.log('final medicibe object for controlled drug',this.med_service.medicine_obj)


    },

    complete : ()=>{
      console.log('copleted the  recognized face')

    }


  })
  // subscribe close 


}
// onsubmit close 

}
// export class close 
