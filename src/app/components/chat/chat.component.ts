import { Component, OnInit } from '@angular/core';
import { TawkService } from '../../services/tawk.service';
import { UserSigninServiceService } from '../../services/user-signin-service.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  userDetails: any;

  constructor(private TawkService: TawkService,private signin_service: UserSigninServiceService) { }

  ngOnInit(): void {
    this.signin_service.getProfile().subscribe({

      next: (result) => {
        //console.log(result)
        //console.log(result['user']);
        this.userDetails = result['user'];
      },

      error: (err) => {
        console.log('unsuccessfull in fetching details from backend in user userprofile component   : ' + err)

      },

      complete: () => (
        console.log('successfully fetched the details from backend in useprofile component'+this.userDetails),
        this.TawkService.UpdateTawkUser(this.userDetails))


    })
  }

}
