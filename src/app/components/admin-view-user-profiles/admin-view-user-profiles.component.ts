import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AdminserviceService } from '../../services/adminservice.service';

@Component({
  selector: 'app-admin-view-user-profiles',
  templateUrl: './admin-view-user-profiles.component.html',
  styleUrls: ['./admin-view-user-profiles.component.css']
})
export class AdminViewUserProfilesComponent implements OnInit {

  userDetails : any =[];
  loader : boolean;

  constructor(private admin_service : AdminserviceService , private router : Router) { }

  ngOnInit(): void {

    this.getAllUserProfile();

  }
  // onit 

  getAllUserProfile(){
  
    this.loader = true;
    this.admin_service.getUsersProfile().subscribe({

      next: (result) => {
        this.loader = false;

        console.log(result)
        this.userDetails = result['user'];
        console.log(this.userDetails);

      },

      error: (err) => {
        this.loader = false;

        console.log('unsuccessfull in fetching all user profile details from backend in user     : ' + err)

      },

      complete: () => (console.log('cpmpleted fetched the all user profile details  details from backend in  component'))



    })
    // subscribe 

  }
  // get user profile 

  getUserPrevOrderHistory(email : string){

    console.log('userselected :  ',email);
    // this.admin_service.userSelectedByAdmin(email);

    this.router.navigate(['/adminViewUserProfile' , email]);




  }
  // getUserPrevOrderHistory 


}
// export class 
