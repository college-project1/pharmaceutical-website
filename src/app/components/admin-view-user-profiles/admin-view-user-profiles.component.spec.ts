import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewUserProfilesComponent } from './admin-view-user-profiles.component';

describe('AdminViewUserProfilesComponent', () => {
  let component: AdminViewUserProfilesComponent;
  let fixture: ComponentFixture<AdminViewUserProfilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminViewUserProfilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminViewUserProfilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
