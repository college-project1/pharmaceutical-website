import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserSigninServiceService } from '../../services/user-signin-service.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  userDetails: any;

  constructor(private router: Router, private signin_service: UserSigninServiceService) { }

  ngOnInit(): void {


    console.log('ng onit of user profile');
    // calling user profile 
    this.signin_service.getProfile().subscribe({

      next: (result) => {
        console.log(result)
        console.log(result['user']);
        this.userDetails = result['user'];
      },

      error: (err) => {
        console.log('unsuccessfull in fetching details from backend in user userprofile component   : ' + err)

      },

      complete: () => (console.log('successfully fetched the details from backend in useprofile component'))


    })

  }

  onLogout() {
    this.signin_service.deleteToken();
    this.router.navigate(['/user_signin']);
  }

}
