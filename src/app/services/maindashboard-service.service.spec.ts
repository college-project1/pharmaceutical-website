import { TestBed } from '@angular/core/testing';

import { MaindashboardServiceService } from './maindashboard-service.service';

describe('MaindashboardServiceService', () => {
  let service: MaindashboardServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MaindashboardServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
