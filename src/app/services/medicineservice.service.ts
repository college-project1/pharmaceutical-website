import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserSigninServiceService } from './user-signin-service.service';
import { of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MedicineserviceService {

  selected_medDetail_obj: any;
  is_home_btn_clicked : boolean;
  prev_page_category: string;

  cartMedicines_id_array: any = [];
  cartMedicines_controlled_array : any =[];
  cartMedicines_uncontrolled_array : any =[];

  name : String;
  total_cart_amount : any;
  medicine_obj : any;

  constructor(private http: HttpClient , private user_signin_service : UserSigninServiceService , private router : Router) { }


  getMedicines(catg_obj: any) {
    console.log('in  medicine  function in service class');
    return this.http.post(environment.apiBaseUrl + '/getMedicines', catg_obj);


  }

  showMedicineDetails(medicine_detail_obj: string) {
    console.log('in show med details in service class')

    this.selected_medDetail_obj = medicine_detail_obj;
    return;

  }

  pageCategory(page_category: string) {

    console.log('prev page category in service class  :  ', page_category)
    this.prev_page_category = page_category;
    console.log('now returing');
    return;

  }


  addMedicinesToCart(med_id: any, med_detail_obj: any) : any
   {

    console.log('inside add med to cart function');

    if (this.cartMedicines_id_array.includes(med_id)) 
    {

      console.log('medicine already exist in cart');  //do nothing
      let string_obj = '{"status": true , "message" : " Medicine already exist in the cart !"}';
      return of(JSON.parse(string_obj));    //of is used to convert anything to OBSERVABLE
     

    }
    // if 


    else {

      console.log('medicine does not exist in cart');
      let temp = {};
      temp = {
        quantity_added: 1,
        total_amount : med_detail_obj['amount'],
        medicine_details: med_detail_obj
      }

      if(med_detail_obj['controlled'] ){

        // first confirm 
        // 1} from prev order
        // 2}verify face

        var payload = this.user_signin_service.getPayload();
        var email_id = payload['email'];


        var info_obj = {
          email : email_id,
          controlled_med_id : med_id
        }
        console.log('info obj sent to prevorder   : ', info_obj);

        return this.http.post(environment.apiBaseUrl + '/getPrevOrders', info_obj).pipe(

              map((result)=>{
                console.log('in map',result);
                console.log(typeof(result));

                if(!result['status'])
                {
                  // now we can purchase med .verify face first 
              


                  this.cartMedicines_controlled_array.push(temp);
                  this.cartMedicines_id_array.push(med_id);
          
                  console.log('new med in controlled cart array  : ',this.cartMedicines_controlled_array );
                  console.log('updated med_id array  : ', this.cartMedicines_id_array);
                  
                }

                else{

                  // if drug is already purchesd in the past 28 days 
                  console.log('drug already purchased in the past 28 days ')
                  return result;

                  

                }

               
                
              return result;
               

              })
              // map ends 

         )
          // pipe 



      }
      // if controlled med 


      else{
        this.cartMedicines_uncontrolled_array.push(temp);
        this.cartMedicines_id_array.push(med_id);

        console.log('new med in uncontrolled cart array  : ',this.cartMedicines_uncontrolled_array );
        console.log('updated med_id array  : ', this.cartMedicines_id_array);

        
        let string_obj = '{"status": false , "message" : "Medicine added to the cart !"}';
        return of(JSON.parse(string_obj));    //of is used to convert anything to OBSERVABLE
      
      }
        // uncontrolled med 

      
     
    }
    // else 



  }
  // addmedto cart 


  deleteCartMedicines() {
    this.cartMedicines_controlled_array = [];
    this.cartMedicines_uncontrolled_array = [];
    this.cartMedicines_id_array = [];
    console.log('medicines and id deleted')
  }

  getMedicinesDetails(){
  
   console.log('fetching details',this.cartMedicines_controlled_array);
    return this.cartMedicines_controlled_array;
  }



  finalCartMedicineDetails(name : String , total_cart_amount : any , med_obj : any){

    console.log('inside finalCartMedicineDetails()');
        
    this.name = name;
    this.total_cart_amount = total_cart_amount;
    this.medicine_obj = med_obj;

    console.log(this.name , this.total_cart_amount , this.medicine_obj);

    console.log('returning from finalCartMedicineDetails() present in service clss');


  }
// fibal med details end s 

  paymentFor_UncontrolledMedicine(){

    console.log('in  paymentFor_UncontrolledMedicine() in service class');
    console.log('storing  below : ')
    console.table(this.medicine_obj);
    return this.http.post(environment.apiBaseUrl + '/storing_and_payment', this.medicine_obj);

  }
  // paymentFor_UncontrolledMedicine end 


  storePendingStatusDetails_forAdminVerification(medicine_obj : any){

    console.log('in  storePendingStatusDetails_forAdminVerification  function in service class');
    return this.http.post(environment.apiBaseUrl + '/PendingStatusDetails_forAdminVerification', medicine_obj );






  }
  // storePendingStatusDetails_forAdminVerification ends 



  showHomepage(val : boolean){
    this.is_home_btn_clicked = val;
  }



}
