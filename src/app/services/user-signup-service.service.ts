import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule , HttpHeaders} from '@angular/common/http';
import { User_Signup_Class } from '../models/user_signup';
import { environment } from '../../environments/environment';



@Injectable({
  providedIn: 'root'
})
export class UserSignupServiceService {

  // a property (NoAuth : True) is being added to the object which the headers property of the request points
  noAuthHeader = { headers: new HttpHeaders({ 'NoAuth': 'True' }) };
  
  // url_states_cities = `https://raw.githubusercontent.com/bhanuc/indian-list/master/state-city.json`;
 


  constructor(private http: HttpClient) { }

  
  postUser(user : User_Signup_Class)
  {
  
    return this.http.post(environment.apiBaseUrl+'/register',user , this.noAuthHeader);


  }
// postuser() close 






}
