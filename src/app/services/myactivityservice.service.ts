import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MyactivityserviceService {

  constructor(private http : HttpClient) { }

getPrevOrderDetails(email_id : String){

  console.log('in  getPrevOrderDetails  function in service class');
    return this.http.post(environment.apiBaseUrl + '/getPrevOrderDetails', {email : email_id});


}

getPendingMedicines(email_obj : any){

  console.log('in  getPrevOrderDetails  function in service class');
    return this.http.post(environment.apiBaseUrl + '/getUserPendingOrderDetails', email_obj);

}

getApprovedMedicines(email_obj : any){

  console.log('in  getPrevOrderDetails  function in service class');
    return this.http.post(environment.apiBaseUrl + '/getUserApprovedOrderDetails', email_obj);

}

getCancelledMedicines(email_obj : any){

  console.log('in  getPrevOrderDetails  function in service class');
    return this.http.post(environment.apiBaseUrl + '/getUserCancelledOrderDetails', email_obj);

}

convertAndStoreApprovedDetailsToCompleteDetails(temp_obj : any , _id : any){
  console.log('in  convertAndStoreApprovedDetailsToCompleteDetails  function in service class');
  return this.http.post(environment.apiBaseUrl + '/StoreApprovedDetailsToCompleteDetails', {temp_obj : temp_obj , _id : _id});

}


}
