import { TestBed } from '@angular/core/testing';

import { FaceserviceService } from './faceservice.service';

describe('FaceserviceService', () => {
  let service: FaceserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FaceserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
