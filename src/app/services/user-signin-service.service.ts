import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User_Signin } from '../models/user_signin';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UserSigninServiceService {

  // a property (NoAuth : True) is being added to the object which the headers property of the request points
  noAuthHeader = { headers: new HttpHeaders({ 'NoAuth': 'True' }) };

  constructor(private http: HttpClient) { }



getProfile(){
  console.log('isdide userprofile service vlass to fetch data fron backend');

  return this.http.get(environment.apiBaseUrl + '/userProfile');
}

  userAuthenticate(user: User_Signin) {
    return this.http.post(environment.apiBaseUrl + '/authenticate', user , this.noAuthHeader);
  }

// Helper methods 

  storeToken(token: string) {
    localStorage.setItem('token', token);
  }

  deleteToken() {
    localStorage.removeItem('token');
  }

  getToken() {
    return localStorage.getItem('token');
  }

  getPayload() {
    var token = this.getToken();
    if (!token) {
      return null;
    }

    else if (token) {
      // decode the payload using atob('base64paylod only') to get the decoded version of payload containing _id ,iat, exp properties as object but returned in string format 
      var payload = atob(token.split('.')[1]);
      // parse the string in json format => to get the object containing those 3 properties 
      return JSON.parse(payload);
    }
  }

  // if token/payload exists and the expiration time > current time => logged in  
   isLoggedIn() {
    var payload = this.getPayload();
    if (payload) {
      // returns true or false 
      return payload.exp > Date.now() / 1000;
     }
     else{
       return false;
     }

  }





}
