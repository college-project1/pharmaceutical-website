import { TestBed } from '@angular/core/testing';

import { AuthenticateprivateresourcesService } from './authenticateprivateresources.service';

describe('AuthenticateprivateresourcesService', () => {
  let service: AuthenticateprivateresourcesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthenticateprivateresourcesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
