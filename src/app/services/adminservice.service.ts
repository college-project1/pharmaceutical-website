import { HttpClient , HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AdminserviceService {

   // a property (NoAuth : True) is being added to the object which the headers property of the request points
   noAuthHeader = { headers: new HttpHeaders({ 'NoAuth': 'True' }) };
  

  constructor(private http : HttpClient) { }

  postUser(admin : any)
  {
  
    return this.http.post(environment.apiBaseUrl+'/admin_register', admin , this.noAuthHeader);


  }



  adminAuthenticate(admin : any){

    return this.http.post(environment.apiBaseUrl+'/admin_authenticate', admin , this.noAuthHeader);

    

  }

  getPendingMedicineDetails()
  {
    console.log('in getPendingMedicineDetails() in adminservice class ');
    return this.http.get(environment.apiBaseUrl+'/getPendingMedicineDetails'  , this.noAuthHeader);
    
  }


  approveOrder(order_obj : any){

    console.log('in approve order()() in adminservice class ');
    return this.http.post(environment.apiBaseUrl+'/approveOrder',order_obj, this.noAuthHeader);
    

  }


  cancelOrder(order_obj : any ){

    console.log('in cancel order()() in adminservice class ');
    return this.http.post(environment.apiBaseUrl+'/cancelOrder',order_obj , this.noAuthHeader);
    

  }


  getUsersProfile(){

    console.log('in  getUsersProfile) in adminservice class ');
    return this.http.get(environment.apiBaseUrl+'/getAllUsersProfile' , this.noAuthHeader);
    

  }

  getUserCompletedStatusDetails(email: string){
    console.log('in  getUsersProfile) in adminservice class ');
    return this.http.post(environment.apiBaseUrl+'/getUserCompletedStatusDetails' , {email : email} , this.noAuthHeader);
    
  }

  updateStatusAndDate(temp_obj : any)
  {
    console.log('in  updateStatusAndDate in adminservice class ');
    return this.http.post(environment.apiBaseUrl+'/updateStatusAndDate_ByAdmin' , temp_obj , this.noAuthHeader);
    

  }




}
