
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FaceserviceService {

  constructor(private http : HttpClient) { }

  getFace_Authenticate()
{
  console.log('in  face authenticating function in service class');
   return this.http.get(environment.apiBaseUrl + '/face');

  
}



detectFace(face_image : any){
  
  console.log('send image for detection from service class');
  return this.http.post(environment.apiBaseUrl + '/detectFace' , face_image).pipe(
    map((data)=>{
      
      console.log('data in service of detect face'+ data);
      return data;
    })
  )

}
// detect face close 


recognizeFace(face_details : any){

  return this.http.post(environment.apiBaseUrl + '/recognizeFace' , face_details).pipe(
    map((data)=>{
      
      console.log('data inside of recognize face service class'+ data);
      return data;
    })
  )

}
// recognize face cloae 


}
// exportcalss close 
