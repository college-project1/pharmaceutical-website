import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthenticateprivateresourcesService {

  constructor(private http : HttpClient) { }


  Authenticate()
  {
    console.log('in   authenticating function in service class ');
     return this.http.get(environment.apiBaseUrl + '/authenticate');
  
    
  }


}


