import { TestBed } from '@angular/core/testing';

import { MyactivityserviceService } from './myactivityservice.service';

describe('MyactivityserviceService', () => {
  let service: MyactivityserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MyactivityserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
