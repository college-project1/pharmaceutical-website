import { DrugDetailsComponent } from './components/drug-details/drug-details.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserSignupComponent } from './components/user-signup/user-signup.component';
import { UserSigninComponent } from './components/user-signin/user-signin.component';
import { HomeComponent } from './components/home/home.component';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';
import { ContactComponent } from './components/contact/contact.component';
import { AboutComponent } from './components/about/about.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { PharmacyMainDashboardComponent } from './components/pharmacy-main-dashboard/pharmacy-main-dashboard.component';
import { AuthGuard } from './auth/auth.guard';
import { RegisterFaceComponent } from './components/register-face/register-face.component';
import { RecognizeFaceComponent } from './components/recognize-face/recognize-face.component';
import { CartComponent } from './components/cart/cart.component';
import { IntermediateformComponent } from './components/intermediateform/intermediateform.component';
import { DashboardDisplayComponent } from './components/dashboard-display/dashboard-display.component';
import { OrderMedicinesComponent } from './components/order-medicines/order-medicines.component';
import { OrderFitnessComponent } from './components/order-fitness/order-fitness.component';
import { OrderPersonalcareComponent } from './components/order-personalcare/order-personalcare.component';
import { OrderMomandbabyComponent } from './components/order-momandbaby/order-momandbaby.component';
import { OrderSexualwellnessComponent } from './components/order-sexualwellness/order-sexualwellness.component';
import { OrderDevicesComponent } from './components/order-devices/order-devices.component';
import { OrderCovidessentialsComponent } from './components/order-covidessentials/order-covidessentials.component';
import { MyactivityComponent } from './components/myactivity/myactivity.component';
import { AdminSigninComponent } from './components/admin-signin/admin-signin.component';
import { AdminSignupComponent } from './components/admin-signup/admin-signup.component';
import { AdminViewPageComponent } from './components/admin-view-page/admin-view-page.component';
import { AdminViewUserProfilesComponent } from './components/admin-view-user-profiles/admin-view-user-profiles.component';
import { AdminCompleteOrderStatusComponent } from './components/admin-complete-order-status/admin-complete-order-status.component';
import { CovidHomeComponent } from './components/covid-home/covid-home.component';
import { CovidCountrywiseComponent } from './components/covid-countrywise/covid-countrywise.component';



const routes: Routes = [

  { path: '', redirectTo: '', pathMatch: 'full' },


  { path: 'user_signup', component: UserSignupComponent },
  { path: 'user_signin', component: UserSigninComponent },
  { path: 'home', component: HomeComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'about', component: AboutComponent },
  {path : 'userProfile' , component : UserProfileComponent , canActivate :[AuthGuard]},
  {path : 'pharmacyMainDashboard' , component : PharmacyMainDashboardComponent , canActivate :[AuthGuard]  },
  {path : 'face' , component : RegisterFaceComponent , canActivate :[AuthGuard]},
  {path : 'recognizeFace' , component : RecognizeFaceComponent , canActivate :[AuthGuard]},
  { path: 'cart', component : CartComponent , canActivate :[AuthGuard]  },
  { path: 'intermediateForm', component : IntermediateformComponent , canActivate :[AuthGuard] },

   {path : 'medicineDetails' , component: DrugDetailsComponent , canActivate :[AuthGuard] },
   {path : 'dashboardDisplay' , component : DashboardDisplayComponent , canActivate :[AuthGuard]},



   {path : 'orderMedicines' , component:OrderMedicinesComponent , canActivate :[AuthGuard] },

   {path : 'orderFitness' , component : OrderFitnessComponent , canActivate :[AuthGuard]},
   {path : 'orderPersonalcare' , component : OrderPersonalcareComponent , canActivate :[AuthGuard] },
   {path : 'orderMomandbaby' , component : OrderMomandbabyComponent , canActivate :[AuthGuard] },
   {path : 'orderSexualwellness' , component : OrderSexualwellnessComponent , canActivate :[AuthGuard] },
   {path : 'orderDevices' , component : OrderDevicesComponent , canActivate :[AuthGuard] },
   {path : 'orderCovidessentials' , component : OrderCovidessentialsComponent , canActivate :[AuthGuard]},
   {path : 'myActivity' , component : MyactivityComponent , canActivate :[AuthGuard]},




  //  admin related

  { path: 'adminSignin', component: AdminSigninComponent },
  { path: 'adminSignup', component: AdminSignupComponent },
  { path: 'adminViewPage', component: AdminViewPageComponent },
  { path: 'adminViewUserProfile', component: AdminViewUserProfilesComponent },
  { path: 'adminViewUserProfile/:email', component: AdminCompleteOrderStatusComponent },



  //covid related

  { path: 'covidHome', component: CovidHomeComponent },
  { path: 'covidCountrywise', component: CovidCountrywiseComponent },









  { path: '**', component: PagenotfoundComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
